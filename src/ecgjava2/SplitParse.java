/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ecgjava2;

import java.io.IOException;
import java.util.regex.Pattern;

/**
 *
 * @author francispapineau
 */
public class SplitParse {

    /*________________________________________________________________________________*/

    private static String REGEX1 = "[!Lig:TemECGPO]";;
    static String TestInput = "!Lig:100Tem:25.50ECG:29POT:10";
    static public int i = 0;
    static public String Light = "", Temp = "", ECG = "", Pot = "";
    static public double Lightnum = 0.00, Tempnum = 0.00, ECGnum = 0.00, Potnum = 0.00;

    static public void splitVal(String Val) throws IOException{


        Pattern p = Pattern.compile(REGEX1);
        String[] items = p.split(Val);
        for(String s : items) {
            //System.out.println(s);
            //System.out.println(s + " " + i);
            if (i == 5){
                Light = items[5];
                ECGJAVa2View.LightValue.setText(Light);
                if (Light != null && !Light.isEmpty()){
                    Lightnum = Double.parseDouble(Light);
                }
                ECGJAVa2View.LightValue.repaint();
            }
            else if (i == 9){
                Temp = items[9];
                ECGJAVa2View.TympTempValue.setText(Temp);
                if (Temp != null && !Temp.isEmpty()){
                    Tempnum = Double.parseDouble(Temp);
                }
                ECGJAVa2View.TympTempValue.repaint();
            }
            else if (i == 13){
                ECG = items[13];
                ECGJAVa2View.ECGValue.setText(ECG);
                if (ECG != null && !ECG.isEmpty()){
                    ECGnum = Double.parseDouble(ECG);
                }
                ECGJAVa2View.ECGValue.repaint();
            }
            else if (i == 17){
                Pot = items[17];
                ECGJAVa2View.SPO2Value.setText(Pot);
                if (Pot != null && !Pot.isEmpty()){
                    Potnum = Double.parseDouble(Pot);
                }
                ECGJAVa2View.SPO2Value.repaint();
            }
            i++;
        }
        if (Light != null && ECG != null && Temp != null && Pot != null && !Light.isEmpty() && !ECG.isEmpty() && !Temp.isEmpty() && !Pot.isEmpty()){
            String x = Lightnum + "," + ECGnum + "," + Tempnum + "," + Potnum + "\n";
            LogFiles.WriteLogFiles.Writetofile(x);
        }

        i = 0;
    }
}
