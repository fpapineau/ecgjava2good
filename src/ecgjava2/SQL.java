/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 *
 *
 * CHANGE TO FIT SQL SERVER
 *
 *
 *
 */
package ecgjava2;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Administrator
 */
public class SQL extends Thread{

  static String Driver = "net.sourceforge.jtds.jdbc.Driver";
  static String Message = "";
  static String copy = "";
  static boolean connected = false;
  static Connection Connection = null;

    public void main(String args[]){
        SQL Server = new SQL();
        Server.start();
    }

    static public void LoadDriver() throws InstantiationException, IllegalAccessException{
        try {
        Class.forName(Driver).newInstance();
        }
        catch (ClassNotFoundException ex) {}
    }
    static void doWork() throws IOException, InstantiationException, IllegalAccessException, SQLException {
        //Load driver
        LoadDriver();

        //Create connection object
        Connection = DriverManager.getConnection("jdbc:jtds:sqlserver://localhost:1433/", "sa", "");
        ECGJAVa2View.ConnectDB.setText("Conneted To DB");
        connected = true;
    }
    static void closeVitalsConn() throws SQLException{
        Connection.close();
    }
}

